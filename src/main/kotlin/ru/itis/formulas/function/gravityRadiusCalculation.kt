package ru.itis.formulas.function

import kotlin.math.*
import ru.itis.formulas.model.Planet
import ru.itis.formulas.utils.Constant.Companion.GRAVITATIONAL_CONSTANT
import ru.itis.formulas.utils.Constant.Companion.MAGNETIC_CONSTANT
import ru.itis.formulas.utils.SolarSystemPlanets

class GravityRadiusCalculation {

    /**
     * Формула гравитационного радиуса планеты
     * @param planet планета X
     * @return гравитационный радиус планеты X
     */
    fun calculateGravityRadius(
        planet: Planet,
    ): Double {
        return (2 * GRAVITATIONAL_CONSTANT * planet.mass)/(planet.gravitationalAcceleration.pow(2))
    }
}
