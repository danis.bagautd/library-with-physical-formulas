package ru.itis.formulas.function

import ru.itis.formulas.model.Planet
import ru.itis.formulas.utils.Constant
import kotlin.math.*

/**
 * Вычисление ускорения свободного падения на планете Х
 * @return ускорение свободного падения на планете Х
 */
fun Planet.calculateGravitationalAcceleration() =
    (Constant.GRAVITATIONAL_CONSTANT * mass) / (radius * radius)

/**
 * Вычисление первой космической скорости для планеты X
 * @return первая космическая скорость планеты X
 */
fun Planet.calculateFirstSpaceVelocity(): Double {
    return (Constant.GRAVITATIONAL_CONSTANT * this.mass)/(this.radius)
}

/**
 * Вычисление второй космической скорости для планеты X
 * @param height высота над поверхностью планеты X, для которой вычисляется скорость
 * @return вторая космическая скорость планеты X
 */
fun Planet.calculateSecondSpaceVelocity(): Double {
    return 2 * Constant.GRAVITATIONAL_CONSTANT * (this.mass / this.radius)
}

/**
 * Функция конвертации первой космической скорости во вторую
 * @return первая космическая скорость планеты X
 */
fun Planet.calculateFirstSpaceVelocityBySecond():Double {
    return secondSpaceVelocity.div(sqrt(2.0))
}

/**
 * Функция конвертации второй космической скорости в первую
 * @return вторая космическая скорость планеты X
 */
fun Planet.calculateSecondSpaceVelocityByFirst(): Double {
    return firstSpaceVelocity.times(sqrt(2.0))
}

/**
 * Функция расчета плотности планеты
 * @return плотность планеты
 */
fun Planet.calculatePlanetDensity(): Double {
    return this.mass / this.volume
}

/**
 * Функция орибитального периорда планеты
 * @return орбитальный период планеты
 */
fun Planet.calculateOrbitalPeriod(): Double {
    return (math.PI * 2 * math.sqrt((this.orbitSemiaxis.pow(3)/(GRAVITATIONAL_CONSTANT*(this.starMass + this.mass)))))
}
