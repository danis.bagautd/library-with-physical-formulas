package ru.itis.formulas.function

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import ru.itis.formulas.utils.SolarSystemPlanets

class WeightCalculationTest {

    private val weightCalculation = WeightCalculation()

    @Test
    fun calculateWeight() {
        val resultEarth = weightCalculation.calculateWeight(SolarSystemPlanets.EARTH.planet, 12.12)
        assertEquals(resultEarth, 12.12)
        val resultJupiter = weightCalculation.calculateWeight(SolarSystemPlanets.JUPITER.planet, 3445.32)
        assertEquals(resultJupiter, 9113.874442722457)
        val resultMars = weightCalculation.calculateWeight(SolarSystemPlanets.MARS.planet, 1327.218)
        assertEquals(resultMars, 500.51610100942895)
        val resultVenus = weightCalculation.calculateWeight(SolarSystemPlanets.VENUS.planet, 24123.22)
        assertEquals(resultVenus, 21839.306582872665)
        val resultSun = weightCalculation.calculateWeight(SolarSystemPlanets.SUN.planet, -123.22)
        assertEquals(resultSun, -3470.445494595628)
        val resultMercury = weightCalculation.calculateWeight(SolarSystemPlanets.MERCURY.planet, 0.0)
        assertEquals(resultMercury, 0.0)
        val resultSaturn = weightCalculation.calculateWeight(SolarSystemPlanets.SATURN.planet, -123.123)
        assertEquals(resultSaturn, -140.55999359386146)
        val resultUranus = weightCalculation.calculateWeight(SolarSystemPlanets.URANUS.planet, -365.148)
        assertEquals(resultUranus, -335.69122007220625)
        val resultNeptune = weightCalculation.calculateWeight(SolarSystemPlanets.NEPTUNE.planet, 567.345)
        assertEquals(resultNeptune, 652.7829926449926)
        val resultPluto = weightCalculation.calculateWeight(SolarSystemPlanets.PLUTO.planet, 765.43)
        assertEquals(resultPluto, 47.57539266413565)
        val resultMoon = weightCalculation.calculateWeight(SolarSystemPlanets.MOON.planet, 0.0)
        assertEquals(resultMoon, 0.0)
    }

    @Test
    fun calculateWeightWithAtmosphere() {
        val calculateAtmposphereEarth = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.EARTH.planet, 1.2, 3.2)
        assertEquals(calculateAtmposphereEarth, -3.9980590693154044E7)
        val calculateAtmosphereSun = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.SUN.planet, 0.0, 0.0)
        assertEquals(calculateAtmosphereSun, 0.0)
        val calculateAtmosphereMoon = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.MOON.planet, -344.243, -56.567)
        assertEquals(calculateAtmosphereMoon, -22.984228751779092)
        val calculateAtmosphereMercury = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.MERCURY.planet, 765.545, 825.334)
        assertEquals(calculateAtmosphereMercury, 287.972352389164)
        val calculateAtmosphereVenus = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.MERCURY.planet, 723.765123, -123123.2323)
        assertEquals(calculateAtmosphereVenus, 272.25616397147473)
        val calculateAtmosphereMars = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.MARS.planet, 23423.34, -654.333)
        assertEquals(calculateAtmosphereMars, 4.8354487093749195E7)
        val calculateAtmosphereJupiter = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.JUPITER.planet, -654345.455, -7654.5)
        assertEquals(calculateAtmosphereJupiter, -1730934.22934186)
        val calculateAtmosphereSaturn = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.SATURN.planet, 76.5, -23424.444)
        assertEquals(calculateAtmosphereSaturn, 4.977350921561522E10)
        val calculateAtmosphereUranus = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.URANUS.planet, -5435.45, 0.0)
        assertEquals(calculateAtmosphereUranus, -4996.967920244594)
        val calculateAtmosphereNeptune = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.NEPTUNE.planet, 0.0, -3564.654)
        assertEquals(calculateAtmosphereNeptune, 1.8080245498161755E10)
        val calculateAtmospherePluto = weightCalculation.calculateWeightWithAtmosphere(SolarSystemPlanets.PLUTO.planet, 0.0, 0.0)
        assertEquals(calculateAtmospherePluto, 0.0)

    }

    @Test
    fun calculateWeightWithCentrifugalForce() {
        val calculateCentrifugalOne = weightCalculation.calculateCentrifugalForce(1.2, 1.2, 1.2)
        assertEquals(calculateCentrifugalOne, 2.0736)
        val calculateCentrifugalTwo = weightCalculation.calculateCentrifugalForce(0.0, 0.0, 0.0)
        assertEquals(calculateCentrifugalTwo, 0.0)
        val calculateCentrifugalThree = weightCalculation.calculateCentrifugalForce(-1.2, 0.0, 465.43)
        assertEquals(calculateCentrifugalThree, -0.0)
        val calculateCentrifugalFour = weightCalculation.calculateCentrifugalForce(-355.2, -10.2, - 101.2)
        assertEquals(calculateCentrifugalFour, 3739846.8095999993)
        val calculateCentrifugalFive = weightCalculation.calculateCentrifugalForce(100.2, -30.4, 565.4)
        assertEquals(calculateCentrifugalFive, 5.235651041279999E7)

    }

    @Test
    fun calculateWeightOnPlanetByGravitationalAcceleration() {
        val weightOnAccelerationEarth = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(1.2, SolarSystemPlanets.EARTH.planet)
        assertEquals(weightOnAccelerationEarth, 1.2)
        val weightOnAccelerationMercury = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(-654.545, SolarSystemPlanets.MERCURY.planet)
        assertEquals(weightOnAccelerationMercury, -246.21787536273544)
        val weightOnAccelerationVenus = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(13443.76543, SolarSystemPlanets.VENUS.planet)
        assertEquals(weightOnAccelerationVenus, 12170.950430912413)
        val weightOnAccelerationMars = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(76543.0101, SolarSystemPlanets.MARS.planet)
        assertEquals(weightOnAccelerationMars, 28865.64903036075)
        val weightOnAccelerationJupiter = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(-6543.101, SolarSystemPlanets.JUPITER.planet)
        assertEquals(weightOnAccelerationJupiter, -17308.407050738897)
        val weightOnAccelerationSaturn = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(3.101876, SolarSystemPlanets.SATURN.planet)
        assertEquals(weightOnAccelerationSaturn, 3.541171598230652)
        val weightOnAccelerationUranus = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(-76543.1984, SolarSystemPlanets.URANUS.planet)
        assertEquals(weightOnAccelerationUranus, -70368.39763363058)
        val weightOnAccelerationNeptune = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(43.0, SolarSystemPlanets.NEPTUNE.planet)
        assertEquals(weightOnAccelerationNeptune, 49.475484376763134)
        val weightOnAccelerationPluto = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(-543.01, SolarSystemPlanets.PLUTO.planet)
        assertEquals(weightOnAccelerationPluto, -33.75085111708752)
        val weightOnAccelerationMoon = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(7653.1010898, SolarSystemPlanets.MOON.planet)
        assertEquals(weightOnAccelerationMoon, 510.9780768481947)
        val weightOnAccelerationSun = weightCalculation.calculateWeightOnPlanetByGravitationalAcceleration(0.01, SolarSystemPlanets.SUN.planet)
        assertEquals(weightOnAccelerationSun, 0.2816462826323347)
    }

    @Test
    fun calculateWeightOnPlanetByFirstSpaceVelocity() {
        val weightByFirstSpaceVelocityEarth = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(1345.2123123,SolarSystemPlanets.EARTH.planet)
        assertEquals(weightByFirstSpaceVelocityEarth, 1.9345295390962297E27)
        val weightByFirstSpaceVelocityMERCURY = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(0.276543,SolarSystemPlanets.MERCURY.planet)
        assertEquals(weightByFirstSpaceVelocityMERCURY, 5.627394233901136E22)
        val weightByFirstSpaceVelocityVENUS = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(12341.2875,SolarSystemPlanets.VENUS.planet)
        assertEquals(weightByFirstSpaceVelocityVENUS, 1.4546286480890347E28)
        val weightByFirstSpaceVelocityMARS = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(87651.28765,SolarSystemPlanets.MARS.planet)
        assertEquals(weightByFirstSpaceVelocityMARS, 1.7926454860702944E28)
        val weightByFirstSpaceVelocityJUPITER = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(7651.2087,SolarSystemPlanets.JUPITER.planet)
        assertEquals(weightByFirstSpaceVelocityJUPITER, 7.699482428414926E28)
        val weightByFirstSpaceVelocitySATURN = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(-16.298765,SolarSystemPlanets.SATURN.planet)
        assertEquals(weightByFirstSpaceVelocitySATURN, -3.0548107446754847E25)
        val weightByFirstSpaceVelocityURANUS = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(-0.234,SolarSystemPlanets.URANUS.planet)
        assertEquals(weightByFirstSpaceVelocityURANUS, -2.844084541718217E23)
        val weightByFirstSpaceVelocityNEPTUNE = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(0.0,SolarSystemPlanets.NEPTUNE.planet)
        assertEquals(weightByFirstSpaceVelocityNEPTUNE, 0.0)
        val weightByFirstSpaceVelocityPLUTO = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(0.00000023,SolarSystemPlanets.PLUTO.planet)
        assertEquals(weightByFirstSpaceVelocityPLUTO, 1.2778096722516312E15)
        val weightByFirstSpaceVelocityMOON = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(-0.000006023,SolarSystemPlanets.MOON.planet)
        assertEquals(weightByFirstSpaceVelocityMOON, -3.861242075711611E16)
        val weightByFirstSpaceVelocitySUN = weightCalculation.calculateWeightOnPlanetByFirstSpaceVelocity(-4431.2,SolarSystemPlanets.SUN.planet)
        assertEquals(weightByFirstSpaceVelocitySUN, -5.0549158591745853E30)
    }

    @Test
    fun calculateWeightOnPlanetBySecondSpaceVelocity() {
        val weightBySecondSpaceVelocityEarth = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(1345.2123123,SolarSystemPlanets.EARTH.planet)
        assertEquals(weightBySecondSpaceVelocityEarth, 1.2338700234491226E31)
        val weightBySecondSpaceVelocityMERCURY = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(0.276543,SolarSystemPlanets.MERCURY.planet)
        assertEquals(weightBySecondSpaceVelocityMERCURY, 1.3725214536484873E26)
        val weightBySecondSpaceVelocityVENUS = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(12341.2875,SolarSystemPlanets.VENUS.planet)
        assertEquals(weightBySecondSpaceVelocityVENUS, 8.802685263910796E31)
        val weightBySecondSpaceVelocityMARS = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(87651.28765,SolarSystemPlanets.MARS.planet)
        assertEquals(weightBySecondSpaceVelocityMARS, 6.090333774375219E31)
        val weightBySecondSpaceVelocityJUPITER = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(7651.2087,SolarSystemPlanets.JUPITER.planet)
        assertEquals(weightBySecondSpaceVelocityJUPITER, 5.382785160529161E33)
        val weightBySecondSpaceVelocitySATURN = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(-16.298765,SolarSystemPlanets.SATURN.planet)
        assertEquals(weightBySecondSpaceVelocitySATURN, -1.7788773928394285E30)
        val weightBySecondSpaceVelocityURANUS = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(-0.234,SolarSystemPlanets.URANUS.planet)
        assertEquals(weightBySecondSpaceVelocityURANUS, -7.213167214705742E27)
        val weightBySecondSpaceVelocityNEPTUNE = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(0.0,SolarSystemPlanets.NEPTUNE.planet)
        assertEquals(weightBySecondSpaceVelocityNEPTUNE, 0.0)
        val weightBySecondSpaceVelocityPLUTO = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(0.00000023,SolarSystemPlanets.PLUTO.planet)
        assertEquals(weightBySecondSpaceVelocityPLUTO, 1.5269825583406996E18)
        val weightBySecondSpaceVelocityMOON = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(-0.000006023,SolarSystemPlanets.MOON.planet)
        assertEquals(weightBySecondSpaceVelocityMOON, -1.0567833437015114E20)
        val weightBySecondSpaceVelocitySUN = weightCalculation.calculateWeightOnPlanetBySecondSpaceVelocity(-4431.2,SolarSystemPlanets.SUN.planet)
        assertEquals(weightBySecondSpaceVelocitySUN, -3.515693980055925E36)
    }

    @Test
    fun calculateWeightOnPlanetByGravitationalPotential() {
        val weightByGravityPotentialEarth = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(1.2, SolarSystemPlanets.EARTH.planet, 12.32)
        assertEquals(weightByGravityPotentialEarth, 1.5091733429372197E-6)
        val weightByGravityPotentialMERCURY = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(6541.2, SolarSystemPlanets.MERCURY.planet, 12356.077)
        assertEquals(weightByGravityPotentialMERCURY, 21.93335843932895)
        val weightByGravityPotentialVENUS = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(7651.2876, SolarSystemPlanets.VENUS.planet, 0.0)
        assertEquals(weightByGravityPotentialVENUS, 0.0)
        val weightByGravityPotentialMARS = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(7651.276, SolarSystemPlanets.MARS.planet, -187652.32)
        assertEquals(weightByGravityPotentialMARS, -388.6504619811904)
        val weightByGravityPotentialJUPITER = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(-7651.2, SolarSystemPlanets.JUPITER.planet, -87098712.32)
        assertEquals(weightByGravityPotentialJUPITER, 25716.68494946957)
        val weightByGravityPotentialSATURN = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(-0.23456, SolarSystemPlanets.SATURN.planet, 987612.398762)
        assertEquals(weightByGravityPotentialSATURN, -0.020714054680724122)
        val weightByGravityPotentialURANUS = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(0.29876, SolarSystemPlanets.URANUS.planet, 8765412.398762)
        assertEquals(weightByGravityPotentialURANUS, 0.2907842768780645)
        val weightByGravityPotentialNEPTUNE = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(0.0, SolarSystemPlanets.NEPTUNE.planet, 0.0000002)
        assertEquals(weightByGravityPotentialNEPTUNE, 0.0)
        val weightByGravityPotentialPLUTO = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(0.0000000003, SolarSystemPlanets.PLUTO.planet, 0.300002)
        assertEquals(weightByGravityPotentialPLUTO, 1.478140357776766E-16)
        val weightByGravityPotentialMOON = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(-2345671.254, SolarSystemPlanets.MOON.planet, -17652.372)
        assertEquals(weightByGravityPotentialMOON, 63307.122042103016)
        val weightByGravityPotentialSUN = weightCalculation.calculateWeightOnPlanetByGravitationalPotential(-76541.2, SolarSystemPlanets.SUN.planet, -12.32)
        assertEquals(weightByGravityPotentialSUN, 0.0034178194955745245)
    }

    @Test
    fun calculateWeightOnPlanetByGasDensity() {
        val weightByGasEARTH = weightCalculation.calculateWeightOnPlanetByGasDensity(7654.12,SolarSystemPlanets.EARTH.planet, -6543.53, 123.233)
        assertEquals(weightByGasEARTH, -92984.16086312014)
        val weightByGasMERCURY = weightCalculation.calculateWeightOnPlanetByGasDensity(765874.12,SolarSystemPlanets.MERCURY.planet, -676543543.53, -123.233)
        assertEquals(weightByGasMERCURY, 33.85068937020782)
        val weightByGasVENUS = weightCalculation.calculateWeightOnPlanetByGasDensity(-654.12,SolarSystemPlanets.VENUS.planet, -6543.53, 0.233)
        assertEquals(weightByGasVENUS, 3804922.409814659)
        val weightByGasMARS = weightCalculation.calculateWeightOnPlanetByGasDensity(76594.12876,SolarSystemPlanets.MARS.planet, 0.53, 0.233)
        assertEquals(weightByGasMARS, 2.29135462171223E12)
        val weightByGasJUPITER = weightCalculation.calculateWeightOnPlanetByGasDensity(-0.12,SolarSystemPlanets.JUPITER.planet, 0.53, 0.233)
        assertEquals(weightByGasJUPITER, -2.5181161581898004E7)
        val weightByGasSATURN = weightCalculation.calculateWeightOnPlanetByGasDensity(0.0000012,SolarSystemPlanets.SATURN.planet, 0.53, 123.233)
        assertEquals(weightByGasSATURN, 0.20547250871070255)
        val weightByGasURANUS = weightCalculation.calculateWeightOnPlanetByGasDensity(0.098765,SolarSystemPlanets.URANUS.planet, 652343.53, 123.233)
        assertEquals(weightByGasURANUS, 0.011064290558226992)
        val weightByGasNEPTUNE = weightCalculation.calculateWeightOnPlanetByGasDensity(2347654.12,SolarSystemPlanets.NEPTUNE.planet, 67654543.53, -1243.233)
        assertEquals(weightByGasNEPTUNE, -314.6002349582875)
        val weightByGasPLUTO = weightCalculation.calculateWeightOnPlanetByGasDensity(-234567894.12,SolarSystemPlanets.PLUTO.planet, 65463.53, -4567123.233)
        assertEquals(weightByGasPLUTO, 477.7009979911151)
        val weightByGasMOON = weightCalculation.calculateWeightOnPlanetByGasDensity(79876654.12,SolarSystemPlanets.MOON.planet, 6546543.53, -0.233)
        assertEquals(weightByGasMOON, -3.425070613809576E7)
        val weightByGasSUN = weightCalculation.calculateWeightOnPlanetByGasDensity(-12345.12,SolarSystemPlanets.SUN.planet, 6765.53, -0.29876533)
        assertEquals(weightByGasSUN, 1.6850785286976917E9)
    }

    @Test
    fun calculateWeightOnPlanetByEccentricityAndMomentOfInertia() {
        val weightOnEccentricityEARTH = weightCalculation.calculateWeightOnPlanetByEccentricityAndMomentOfInertia(765.13, SolarSystemPlanets.EARTH.planet,765.4, -123.4, -64.3, 0.4407)
        assertEquals(weightOnEccentricityEARTH, 765.13)
        val weightOnEccentricityURANUS = weightCalculation.calculateWeightOnPlanetByEccentricityAndMomentOfInertia(0.13, SolarSystemPlanets.URANUS.planet,-12.2, -123.4, -623454.3, 1234523.444)
        assertEquals(weightOnEccentricityURANUS, 0.12999999999999998)
        val weightOnEccentricityMARS = weightCalculation.calculateWeightOnPlanetByEccentricityAndMomentOfInertia(5.13, SolarSystemPlanets.MARS.planet,-0.4, 123.4, -64.3, 17423.444)
        assertEquals(weightOnEccentricityMARS, 5.13)
        val weightOnEccentricityMOON = weightCalculation.calculateWeightOnPlanetByEccentricityAndMomentOfInertia(890.08, SolarSystemPlanets.MOON.planet,9876.4, 123.4, -64.3, 12345823.444)
        assertEquals(weightOnEccentricityMOON, 890.08)
        val weightOnEccentricitySUN = weightCalculation.calculateWeightOnPlanetByEccentricityAndMomentOfInertia(-34567.13, SolarSystemPlanets.SUN.planet,-7896.56, -123.4, -64.3, -13.098765)
        assertEquals(weightOnEccentricitySUN, -34567.13)
    }

    @Test
    fun calculateWeightOnPlanetByMagnetic() {
        val weightByMagneticEARTH = weightCalculation.calculateWeightOnPlanetByMagnetic(0.0, SolarSystemPlanets.EARTH.planet, 12.3, 455.5, 0.022, 0.0000000001)
        assertEquals(weightByMagneticEARTH, 0.0)
        val weightByMagneticMOON = weightCalculation.calculateWeightOnPlanetByMagnetic(10.01, SolarSystemPlanets.MOON.planet, -12.30005, -123.3, -0.022, -770.0000000001)
        assertEquals(weightByMagneticMOON, 1.4826471523090378E17)
        val weightByMagneticSUN = weightCalculation.calculateWeightOnPlanetByMagnetic(-12350.00006, SolarSystemPlanets.SUN.planet, 1212.3, 455.5, 0.022, 0.0000000001)
        assertEquals(weightByMagneticSUN, -3.393650066201167E12)
    }

    @Test
    fun calculateArchimedForce() {
        val calculateArchimedOne = weightCalculation.calculateArchimedForce(123.198765, 355.55, 933.3)
        assertEquals(calculateArchimedOne, 4.088163939200348E7)
        val calculateArchimedTwo = weightCalculation.calculateArchimedForce(0.0, 0.0, 0.0)
        assertEquals(calculateArchimedTwo, 0.0)
        val calculateArchimedThree = weightCalculation.calculateArchimedForce(-123.198765, -355.55, -933.3)
        assertEquals(calculateArchimedThree, -4.088163939200348E7)
        val calculateArchimedFour = weightCalculation.calculateArchimedForce(-123.198765, 0.0, 933.3)
        assertEquals(calculateArchimedFour, -0.0)
        val calculateArchimedFive = weightCalculation.calculateArchimedForce(0.00000345, -355.55, 0.000000568)
        assertEquals(calculateArchimedFive, -6.967357799999999E-10)
    }

    @Test
    fun calculateCentrifugalForce() {
        val calculateCentrifugalOne = weightCalculation.calculateCentrifugalForce(0.0,0.0, 0.0)
        assertEquals(calculateCentrifugalOne, 0.0)
        val calculateCentrifugalTwo = weightCalculation.calculateCentrifugalForce(65765.0,87.0, 876550.08765)
        assertEquals(calculateCentrifugalTwo, 4.3632496969675375E14)
        val calculateCentrifugalThree = weightCalculation.calculateCentrifugalForce(0.0002,0.0543, 0.07654)
        assertEquals(calculateCentrifugalThree, 4.513548492000001E-8)
        val calculateCentrifugalFour = weightCalculation.calculateCentrifugalForce(-123450.0,-345670.0, -876540.0)
        assertEquals(calculateCentrifugalFour, 1.29296334508985E22)
    }
}