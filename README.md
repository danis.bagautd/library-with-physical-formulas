# Library with physical formulas

## Description
A complex for calculating the weight of an object for different gases of the atmosphere, different heights, latitudes. As well as the calculation of the first and second cosmic velocities

## Usage
There are several main objects in the library:
1) Planet.kt - Kotlin class of the planet. It contains parameters required for further calculations, such as mass, radius, etc.
2) HelperFunctions.kt - Kotlin file containing auxiliary functions for planet parameters calculation, such as acceleration, cosmic velocity, etc.
3) WeightCalculation.kt - Kotlin class containing all of the main API of the library. Here are functions that let you calculate the weight of an oblect depending on the planet environment.
4) Constant.kt - Kotlin class containing important constanst for calculations.
5) SolarSystemPlanets - Kotlin enum class containing all the Solar system planets' objects.

Firsly, you create a planet object passing mass and radius for the planet. Secondly, you can you HelperFunctions.kt file, but remember that all main parameters already are initialized on startup. Thirdly, you can use WeightCalculation class to get a weight of an object in specific conditions.

## Support
If you've encountered any problems with the library, you can ask a question through an e-mail:
Mokshin Dmitry (dmtr.mkshn@gmail.com)

## Contributing
If you have ideas that can improve the library's functionality, usage experience or documentation, we will be happy to see your contribution.
For that go to the "Issues" tab and create an issue where you describe:
- type of the issue
- name
- description (describe your idea, or steps to reproduce a bug, etc.)
- steps to reproduce (for bugs)
- version of the library (for bugs and improvements)  
In a short period of time we'll answer you and discuss your idea.

## Authors and acknowledgment
Shigapov Rinat (@rinat.shigapov)  
Zakirov Rinat (@rint1k)  
Mokshin Dmitry (@daemon_essa)  
